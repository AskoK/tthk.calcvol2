﻿namespace Calc.Core.Domain
{
    public class ValueCalcNode : CalcNode
    {
        public ValueCalcNode(decimal value)
        {
            Value = value;
        }

        public ValueCalcNode(string value)
        {
            StringValue = value;
        }

        public override decimal GetValue()
        {
            return Value;
        }

        public override string GetString()
        {
            return Value.ToString();
        }

        public decimal Value { get; }
        public string StringValue { get; }
    }

}
