﻿using Calc.Core.ApplicationServices;
using Calc.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Calc.ApplicationServices
{
  public class NodeFactory
  {
    private Dictionary<string, IOperator> _operators = new Dictionary<string, IOperator>();

    public void RegOperator<T>(IOperator @operator)
    {
      _operators[@operator.Symbol] = @operator;
    }

    public bool IsKnowOperator(string s)
    {
      return _operators.ContainsKey(s);
    }

    public IBinaryOperator GetOperator(string op)
    {
      if (IsKnowOperator(op))
      {
        _operators.TryGetValue(op, out var result);
        return (IBinaryOperator) result;
      }

      return null; // Return something better
    }
  }

  public class CalculatorService : ICalculatorService
  {
    private NodeFactory _nodeFactory = new NodeFactory();

    public CalculatorService()
    {
      _nodeFactory.RegOperator<BinaryCalcNode>(new AddOperator());
      _nodeFactory.RegOperator<BinaryCalcNode>(new MultiplyOperator());
      _nodeFactory.RegOperator<BinaryCalcNode>(new DivideOperator());
      _nodeFactory.RegOperator<BinaryCalcNode>(new SubtractOperator());
    }

    public decimal EvalExpression(string expression)
    {
      var node = Parse(expression);
      return node.GetValue();
    }

    public string[] Split(string text)
    {
      var list = new List<string>();

      var currentValue = string.Empty;

      foreach (char c in text)
      {
        var op = c.ToString();
        if (_nodeFactory.IsKnowOperator(op))
        {
          if (!string.IsNullOrEmpty(currentValue))
          {
            list.Add(currentValue);
            currentValue = string.Empty;
          }

          list.Add(op);
        }
        else
        {
          currentValue += c;
        }
      }

      if (!string.IsNullOrEmpty(currentValue))
      {
        list.Add(currentValue);
        currentValue = string.Empty;
      }

      return list.ToArray();
    }

    public CalcNode Parse(string v)
    {
      var list = Split(v).ToList();
      var left = new ValueCalcNode(int.Parse(list[0]));
      if (list.Count >= 2)
      {
        var op = _nodeFactory.GetOperator(list[1]);
        list.RemoveRange(0,2);
        return new BinaryCalcNode(op, left, Parse(string.Join(string.Empty,list)));
      }

      return left;
    }
  }
}